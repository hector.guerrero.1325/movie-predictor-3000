'use strict';

/**
 * @ngdoc function
 * @name moviePredictorApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the moviePredictorApp
 */
angular.module('moviePredictorApp')
  .controller('MainCtrl', function ($http, $interval) {
    var main = this;

    var defaultDirector = { value: -1, text: 'Select a Director' };
    var defaultActor = { value: -1, text: 'Select an Actor' };
    var defaultGenre = { value: -1, text: 'Select a Genre' };
    var defaultPath = 'https://s3-us-west-1.amazonaws.com/tfc-hackaton2016-musketers/result_'

    function makePrediction() {
      main.interval = $interval(function() {
        $http.get(defaultPath + 'prediction_' + main.selectedDirector + '_' + main.selectedActor1 + '_' + main.selectedActor2 + '_' + main.selectedGenre + '.json')
          .success(function(data, status, headers, config) {
            main.contentRating = contentRatings[Math.floor(Math.random() * contentRatings.length)];
            main.movieImdbRating = 1.0;
            main.movieGross = '10dlls';
          });
      }, 2000);

      main.interval2 = $interval(function() {
        $http.get(defaultPath + 'moviename_' + main.selectedDirector + '_' + main.selectedActor1 + '_' + main.selectedActor2 + '_' + main.selectedGenre + '.json')
          .success(function(data, status, headers, config) {
            main.movieName = 'RandomName';
          });
      }, 2000);

      main.endLongPolling = function () {
         $interval.cancel(main.interval);
      }

      main.endLongPolling2 = function() {
        $interval.cancel(main.interval2);
      }
    }

    function getHeaderPosition(data) {
      var positions = {
        director: -1,
        genres: -1,
        actors: []
      };

      data = data[0].split(',');

      data.forEach(function(h, idx) {
        switch(h) {
          case 'director_name':
            positions.director = idx;
            break;
          case 'actor_1_name':
          case 'actor_2_name':
          case 'actor_3_name':
            positions.actors.push(idx);
            break;
          case 'genres': 
            positions.genres = idx;
            break;
        }
      });

      return positions;
    }

    function getDirectors(data, directorPosition) {
      return data[directorPosition];
    }

    function getActors(data, actorPosition) {
      var actors = [];
      actorPosition.forEach(function(i) {
        actors.push(data[i]);
      });
      return actors;
    }

    function getGenres(data, genresPosition) {
      return data[genresPosition];
    }

    function createSelectArrays(directors, actors, genres) {
      var selectValues = {
        directors: [defaultDirector],
        actors: [defaultActor],
        genres: [defaultGenre]
      };

      directors.forEach(function(dir) {
        selectValues.directors.push({ value: dir, text: dir });
      });

      actors.forEach(function(act) {
        selectValues.actors.push({ value: act, text: act });
      });

      genres.forEach(function(gen) {
        selectValues.genres.push({ value: gen, text: gen });
      });

      return selectValues;
    }

    function processCSVData(data) {
      var csvArray = data.split('\n');
      var headerRow = csvArray.splice(0, 1);
      var positions = getHeaderPosition(headerRow);

      var directors = [];
      var actors = [];
      var genres = [];

      csvArray.forEach(function(e) {
        var movieData = e.split(',');
        
        var director = getDirectors(movieData, positions.director);
        if (director && directors.indexOf(director) < 0) {
          directors.push(director);
        }
        
        var genre = getGenres(movieData, positions.genres);
        genre = genre ? genre.split('|') : [];
        genre.forEach(function(gen) {
          if (gen && genres.indexOf(gen) < 0) {
            genres.push(gen);
          }
        });
        
        var actor = getActors(movieData, positions.actors);
        actor.forEach(function(a) {
          if (a && actors.indexOf(a) < 0) {
            actors.push(a);
          }
        });
      });

      var selectValues = createSelectArrays(directors, actors, genres);

      main.directors = selectValues.directors;
      main.actors1 = selectValues.actors;
      main.actors2 = selectValues.actors;
      main.genres = selectValues.genres;
    }

    var contentRatings = [
      'PG',
      'PG-13',
      'R',
      'NC-17'
    ];

    main.selectedDirector = defaultDirector;
    main.selectedActor1 = defaultActor;
    main.selectedActor2 = defaultActor;
    main.selectedGenre = defaultGenre;

    main.makePrediction = makePrediction;
    main.movieName = 'MyMovie';
    main.contentRating = contentRatings[Math.floor(Math.random() * contentRatings.length)];
    main.movieImdbRating = 0.0;
    main.movieGross = 0;

    $http.get('https://s3-us-west-1.amazonaws.com/tfc-hackaton2016-musketers/movie_metadata.csv')
      .then(function(data) {
        processCSVData(data.data);
      })
      .catch(function(error) {
        console.log(error);
      });

  });

'use strict';

/**
 * @ngdoc function
 * @name moviePredictorApp.controller:MaincontrollerCtrl
 * @description
 * # MaincontrollerCtrl
 * Controller of the moviePredictorApp
 */
angular.module('moviePredictorApp')
  .controller('MaincontrollerCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

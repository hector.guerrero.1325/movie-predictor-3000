'use strict';

/**
 * @ngdoc function
 * @name moviePredictorApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the moviePredictorApp
 */
angular.module('moviePredictorApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
